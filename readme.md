***Task 1*** Reentrant lock example  
  
Method 1 called  
...  
Method 2 called  
...  
Method 3 called  
...  
A = 3600000 as expected  
  
***Task 2*** Piped Stream with BlockingQueue  
  
����� ����� �����. � �� ��� ���� ������ ����� BlockingQueue  
  
***Task 3*** Difference between resource usage with and without locking  
  
Expected: 400000. Result without lock: 279353  
Expected: 400000. Result with lock: 400000  