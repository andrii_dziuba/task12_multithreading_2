package task1;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ReentrantLockExample {

    long A = 0;

    Lock lock = new ReentrantLock();

    void method1() {
        lock.lock();
        try {
            System.out.println("Method 1 called");
            A++;
        } finally {
            lock.unlock();
        }
    }

    void method2() {
        lock.lock();
        try {
            System.out.println("Method 2 called");
            A++;
        } finally {
            lock.unlock();
        }
    }

    void method3() {
        lock.lock();
        try {
            System.out.println("Method 3 called");
            A++;
        } finally {
            lock.unlock();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        ReentrantLockExample task = new ReentrantLockExample();
        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 50000; i++)
                task.method1();
        });
        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 50000; i++)
                task.method2();
        });
        Thread t3 = new Thread(() -> {
            for (int i = 0; i < 500000; i++)
                task.method3();
        });
        t1.start();
        t2.start();
        t3.start();

        t1.join();
        t2.join();
        t3.join();

        System.out.println("A = " + task.A + " as expected");
    }

}
