package task3;

public class Main {

    private static long counter = 0;
    private static long counter1 = 0;

    static CustomLock lock = new CustomLock();

    static Runnable run = () -> {
        lock.lock();
        for (int i = 0; i < 100000; i++) {
            counter1++;
        }
        lock.unlock();
    };

    static Runnable runWithoutLock = () -> {
        for (int i = 0; i < 100000; i++) {
            counter++;
        }
    };

    public static void main(String[] args) {
        Thread t1 = new Thread(runWithoutLock);
        Thread t2 = new Thread(runWithoutLock);
        Thread t3 = new Thread(runWithoutLock);
        Thread t4 = new Thread(runWithoutLock);

        t1.start();
        t2.start();
        t3.start();
        t4.start();
        try {
            t1.join(); t2.join(); t3.join(); t4.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Expected: " + 100000 * 4 + ". Result without lock: " + counter);

        Thread _t1 = new Thread(run);
        Thread _t2 = new Thread(run);
        Thread _t3 = new Thread(run);
        Thread _t4 = new Thread(run);

        _t1.start();
        _t2.start();
        _t3.start();
        _t4.start();
        while(_t1.isAlive() || _t2.isAlive() || _t3.isAlive() || _t4.isAlive());

        try {
            _t1.join(); _t2.join(); _t3.join(); _t4.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Expected: " + 100000 * 4 + ". Result with lock: " + counter1);
    }
}
