package task2;

import java.util.concurrent.ArrayBlockingQueue;

public class PipedStream {

    static final ArrayBlockingQueue<Character> queue = new ArrayBlockingQueue(20);

    public static void main(String[] args) {

        new Thread(() -> {
            for (char c : "Дзюба Андрій няшка. Ця вся тема працює через BlockingQueue".toCharArray()) {
                try {
                    queue.put(c);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        new Thread(() -> {
            Character c = null;
            while ((c = queue.poll()) != null) {
                try {
                    Thread.currentThread().sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.print(c);
            }
        }).start();
    }
}
